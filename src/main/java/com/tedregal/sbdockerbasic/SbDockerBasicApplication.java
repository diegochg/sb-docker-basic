package com.tedregal.sbdockerbasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SbDockerBasicApplication {

  public static void main(String[] args) {
    SpringApplication.run(SbDockerBasicApplication.class, args);
  }

  @GetMapping("/greet")
  public String greet(@RequestParam(value = "to", defaultValue = "world") String to) {
    return String.format("Hello %s!", to);
  }

}
